﻿/*
 * Nom : NaziBot
 * Description : Un bot qui réagit à certains messages en répondant avec des messages nazis
 * Auteur(s) : Océane Drezet, Valentin Bouchard
 */

// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
  handleNaziBot: handleNaziBot // permet d'appeler cette méthode dans server.js -> naziBot.handleNaziBot(...)
}

/**
 * Réponds lorsque certains messages sont envoyés, mais peut aussi forcer un utilisateur indésirable à quitter le chat
 */
function handleNaziBot(io, message)
{
	// Passe le message en minuscules (recherche insensible à la casse)
	message = message.toLowerCase();
	
	// Est-ce qu'il contient une référence nazie ?
	if (message.includes('non') || message.includes('no') || message.includes('nope'))
	{
	// Si oui, envoie la réponse de Hitler...
	io.sockets.emit('new_message',
	{
	  name:'Hitler',
	  message:'<span>Nein Nein Nein!!!</span>'
	});
	}
	else if (message.includes('hitler'))
	{
	io.sockets.emit('new_message',
	{
	  name:'Hitler',
	  message:'<span class="knitler imageNaziBot"></span>'
	});
	}
	else if (message.includes('send nudes'))
	{
	io.sockets.emit('new_message',
	{
	  name:'Hitler',
	  message:'<span class="nudes imageNaziBot"></span>'
	});
	// Est-ce qu'il contient un mot interdis ?
	}
	else if (message.includes('dissertation') || message.includes('cours') || message.includes('travail') || message.includes('droit') || message.includes('prof'))
	{
		// Si oui, il le supprime et envoies un message personnalité
		io.sockets.emit('removeLastMessage');
		io.sockets.emit('new_message',
		{
		  name:'Hitler',
		  message:'<span>Nein Nein Nein!!!</span>'
		});
	  }
}
