/*
 * Nom : 
 * Description : 
 * Auteur(s) : Charline
 */

// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
	styleTxt: styleTxt
}

/**
 * 
 */
function styleTxt(ent, message)
{	
	message = message.replace(ent.encode("[b]"), "<strong>");
	message = message.replace(ent.encode("[/b]"), "</strong>");
	message = message.replace(ent.encode("[i]"), "<em>");
	message = message.replace(ent.encode("[/i]"), "</em>");
	message = message.replace(ent.encode("[s]"), "<strike>");
	message = message.replace(ent.encode("[/s]"), "</strike>");
	message = message.replace(ent.encode("[u]"), "<u>");
	message = message.replace(ent.encode("[/u]"), "</u>");
	return message;
}
