/*
 * Nom : avatar
 * Description : Ce module permet d'attribuer un avatar à un utilisateur.
 * Auteur(s) : Grégory Lallé et Julien Tison
 */

// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
	onConnection: onConnection // permet d'appeler cette méthode dans server.js -> avatar.onConnection(...)
}

var socketAvatar;

/**
 * Attribuer un avatar à un utilisateur
 */
function onConnection(socket)
{
    // Stocke le lien de l'avatar par défaut dans l'objet socket
    socket.avatar = "uploads/avatar_default.png";
    
    var fs = require("fs"); // Accès au système de fichier

	// Réception d'un avatar
    socket.on('avatar', function(avatar)
    {
        //  Envoi l'avatar de l'utilisateur dans le dossier uploads
        socket.avatar = "uploads/avatar_" + socket.id + ".png";

        // Créer le fichier image dans le dossier uploads
        fs.writeFile("../client/assets/" + socket.avatar, avatar,
            function complete(error)
            {
                if (error) throw error;
                
                // Garde en mémoire le lien de l'image
                socketAvatar = socket.avatar;

                // Ajout d'un timestamp pour que l'image soit bien chargée
                socket.avatar += '?t=' + Date.now();

                // Indique que l'avatar a été téléchargé
                socket.emit('avatar_loaded', socket.avatar);
            }
        );

    });

    // Déconnexion d'un utilisateur
    socket.on("disconnect", function()
    {
        if(socket.avatar !== "uploads/avatar_default.png")
        {
            // Supprime l'image du dossier uploads
            fs.unlink("../client/assets/" + socketAvatar, (error) => {/* handle error */});
        }
    });
}