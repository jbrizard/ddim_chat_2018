/*
 * Nom : Bot joke !
 * Description : Ce module répond une blaugue quand on appelle joke !
 * Auteur(s) : Grégory Lallé et Julien Tison
 */

// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
	init: init,
	handleBotJoke: handleBotJoke // permet d'appeler cette méthode dans server.js -> botJoke.handleBotJoke(...)
}

var botJokeSpeak = true;

/**
 * Envoie d'une blague toutes les 2 minutes
 */
function init(io)
{
	setInterval(function()
	{
		if(botJokeSpeak)
		{
			sendJoke(io);
		}
	}, 120000);
}

/**
 * Vérifie ce que contient le message envoyé
 */
function handleBotJoke(io, message)
{
	// Passe le message en minuscules (recherche insensible à la casse)
	message = message.toLowerCase();
	
	// Est-ce qu'il contient le groupe une blague ?
	if (message.includes('une blague'))
	{
		sendJoke(io);
	}
	// Est-ce qu'il contient le groupe tg bot ?
	if (message.includes('tg bot'))
	{
		botJokeSpeak = false;
		io.sockets.emit('new_message',
		{
			name:'Bot Joke',
			message:'<span class="message-joke">Je me tais, snif !</span>',
			avatar: "uploads/avatar_jokeSad.png"
		});
	}
}

/**
 * Lorsqu'on demande une blague, il en donne une aléatoirement
 */
function sendJoke(io)
{
	// Tableaux des blagues
	var tableJoke = ["Que fait une fraise sur un cheval ?", "Un jour Dieu dit à Casto de ramer...", "Qu'est-ce qu'une manifestation d'aveugles ?", "L'autre jour, j’ai raconté une blague sur Carrefour...", "Dans la phrase : le voleur a volé une télévision, où est le sujet ?", "Comment appelle-t-on un chat tombé dans un pot de peinture le jour de Noël ?", "Quelle est la capitale de l'île de Tamalou ?"];
	var tableAnswer = ["Tagada, tagada, tagada...", "Et depuis, castorama...", "Un festival de Cannes", "mais elle a pas supermarché...", "En prison !", "Un chat-peint de Noël", "Gébobola !"];

	// Stockage d'un nombre choisi aléatoirement pour donner une blague aléatoire
	var i = Math.floor(Math.random() * tableJoke.length); 

	// Si oui, on envoie une joke de manière aléatoire
	io.sockets.emit('new_message',
	{
		name:'Bot Joke',
		message:'<span class="message-joke">' + tableJoke[i] + '</span>',
		avatar: "uploads/avatar_joke.png"
	});
	setTimeout(function()
	{
		io.sockets.emit('new_message',
		{
			name:'Bot Joke',
			message:'<span class="message-joke">' + tableAnswer[i] + '</span>',
			avatar: "uploads/avatar_joke.png"
		});
	}, 5000);
}