$('#search-gif').click(function()
{
    $('#gif-popup').toggle();
});

$('#search-gif-input').keyup(function()
{    
    var val = $(this).val();
    
    if(val.length<3)
        return

    socket.emit('search_gif',val);
});

socket.on('giphy_results',function(urlGifs)
{
    var result = $('#result-gif');
    console.log("dada");
    result.empty();
    for(var i in urlGifs)
    {
        var iFrame = $('<div class="iframe-container"><iframe class="gif-frame" src="'+urlGifs[i]+'" /></div>');
        result.append(iFrame);
        iFrame.click(onClickiFrame);
        
    }
});

function onClickiFrame()
{
    console.log("click");
    var iframe = $(this).children();
    var src = iframe.attr('src');
    console.log(src);
    socket.emit('message_gif',src);
    
    $('.iframe-container').children().hide();
    $('#gif-popup').toggle();
}