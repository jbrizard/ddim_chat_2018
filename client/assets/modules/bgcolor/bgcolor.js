$(document).ready(function() {
	
	$(".bg-color").click(function (){
		var color = $(this).css("background-color");
		console.log(color);
		
		// Envoi de l'information au server
		socket.emit('bgcolor', color);
	});

	socket.on('change_bgcolor', receiveBgcolor);
	
	function receiveBgcolor(color)
	{
		$('body').css("background-color", color);
	};
	
});