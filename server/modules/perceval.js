﻿/*
 * Nom : Perceval
 * Description : Un bot qui réagit à certains messages en répondant avec des répliques de Perceval de Kaamelott
 * Auteur(s) : Océane Drezet, Valentin Bouchard
 */

// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
  handlePerceval: handlePerceval // permet d'appeler cette méthode dans server.js -> perceval.handlePerceval(...)
}

/**
 * Réponds lorsque certains messages sont envoyés
 */
function handlePerceval(io, message)
{
	// Passe le message en minuscules (recherche insensible à la casse)
	message = message.toLowerCase();

	// Est-ce qu'il contient une référence de Perceval ?
	if (message.includes('13') || message.includes('14') || message.includes('15'))
	{
		// Si oui, envoie la réponse de Perceval...
		io.sockets.emit('new_message',
		{
			name:'Perceval',
			message:"<span>13, 14, 15... Enfin tous les chiffres impairs jusqu'à 22.</span>"
		});
	}
	else if (message.includes('enerve') || message.includes('colere'))
	{
		io.sockets.emit('new_message',
		{
			name:'Perceval',
			message:"<span>On en a gros!!!</span>"
		});
	}
	else if (message.includes('ouai') || message.includes('ouais'))
	{
		io.sockets.emit('new_message',
		{
			name:'Perceval',
			message:"<span>C'est pas faux!</span>"
		});
	}
}
