/*
 * Nom : Giphy
 * Description : Ce module permet de rechercher un gif
 * Auteur(s) : Pierre Durand
 */

// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
    searchGif: searchGif, // permet d'appeler cette méthode dans server.js -> giphy.searchGif(...)
    sendGif: sendGif
}

var api_key = "dc6zaTOxFJmzC";
var endpoint = ("http://api.giphy.com/v1/gifs");
const request = require('request');

/**
 * fonction qui permet de rechercher un gif 
 */
function searchGif(socket, query)
{
    var urlGifs = [];
    // effectue une requete http avec request
    request( endpoint+"/search?q="+ query +"&api_key="+api_key,
        function(err, res, body) 
        {  
            body = JSON.parse(body);
            
            for(var i in body.data)
            {
                if(i>3)
                    break;

                urlGifs[i] = body.data[i].embed_url;
            }

            socket.emit('giphy_results',urlGifs);
        }
    );
}

/**
 * envoie un iFrame
 * @param {source de l'iframe} src 
 */
function sendGif(socket,src)
{
    
    var iFrame = '<iframe src="'+ src +'" />';
    socket.emit('new_message', {name:socket.name, message:iFrame});
}