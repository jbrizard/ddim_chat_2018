/*
 * Nom : Youtube
 * Description : Sert à envoyer une vidéo Youtube avec une simple recherche commençant par "Youtube :"
 * Auteur(s) : Teamjojo (Baptiste, Mélanie, Jordan)
 */

var key = "AIzaSyAl18oTvz_rlMWMdTb_FwabYhcWyoBkBLA";
 
// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
	handleYoutube: handleYoutube // permet d'appeler cette méthode dans server.js -> youtube.handleYoutube(...)
}

/**
 * Lorsqu'on appelle Youtube, il affiche la première vidéo en lien avec la demande
 */
function handleYoutube(io, message)
{
	if (message.toLowerCase().startsWith('y :') || message.toLowerCase().startsWith("youtube"))
	{
		var callSearch = message.indexOf(" ");
		var wordSearch = message.substr(callSearch, message.length);
		var request = require('request');
		
		// On appelle l'API Youtube en récupérant le(s) mot(s) cherché(s)
		request('https://www.googleapis.com/youtube/v3/search?part=snippet&q='+wordSearch+'&key='+key,
			function(err, res, body)
			{  
				// On affiche les données reçues et on récupère l'ID de la première vidéo
				var result = JSON.parse(body); 
				
				if (result.items.length == 0)
				{	
					io.sockets.emit('new_message',
					{
						name:'Youtube',
						message:'Gros tape un vrai truc'
					});
					return;
				}
				
				var idVideo = result.items[0].id.videoId;
				
				// On affiche l'iframe de la vidéo sélectionnée
				var resultVideo = '<iframe width="100%" height="250" src="https://www.youtube.com/embed/'+idVideo+'" allowfullscreen></iframe>';

				io.sockets.emit('new_message',
				{
					name:'Youtube',
					message:resultVideo
				});
			}
		);	
	}	
}


