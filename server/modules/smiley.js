/*
 * Nom : Smiley 
 * Description : Remplacer un smiley clavier en emoji
 * Auteur(s) : Charline
 */

// Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
	replaceSmileys: replaceSmileys
}

/**
 * Lorsque nous écrivons un smiley avec le clavier, il se transforme en emoji.
 */
function replaceSmileys(ent, message)
{
	message = message.replace(ent.encode(":)"), "<span class='smiley-smiling smiley'>:)</span>");
	message = message.replace(ent.encode(":-)"), "<span class='smiley-smiling smiley'>:)</span>");
	message = message.replace(ent.encode("<3"), "<span class='smiley-heart smiley'>:)</span>");
	message = message.replace(ent.encode(":o"), "<span class='smiley-open-mouth smiley'>:)</span>");
	message = message.replace(ent.encode(":-o"), "<span class='smiley-open-mouth smiley'>:)</span>");
	message = message.replace(ent.encode(":O"), "<span class='smiley-open-mouth smiley'>:)</span>");
	message = message.replace(ent.encode(":-O"), "<span class='smiley-open-mouth smiley'>:)</span>");
	message = message.replace(ent.encode(":p"), "<span class='smiley-tongue smiley'>:)</span>");
	message = message.replace(ent.encode(":-p"), "<span class='smiley-tongue smiley'>:)</span>");
	message = message.replace(ent.encode(":P"), "<span class='smiley-tongue smiley'>:)</span>");
	message = message.replace(ent.encode(":-P"), "<span class='smiley-tongue smiley'>:)</span>");
	message = message.replace(ent.encode(":("), "<span class='smiley-frowning-face smiley'>:)</span>");
	message = message.replace(ent.encode(":-("), "<span class='smiley-frowning-face smiley'>:)</span>");
	message = message.replace(ent.encode(":,)"), "<span class='smiley-tears-of-joy smiley'>:)</span>");
	message = message.replace(ent.encode("x("), "<span class='smiley-dizzy-face smiley'>:)</span>");
	message = message.replace(ent.encode("X("), "<span class='smiley-dizzy-face smiley'>:)</span>");
	message = message.replace(ent.encode("X-("), "<span class='smiley-dizzy-face smiley'>:)</span>");
	message = message.replace(ent.encode(";)"), "<span class='smiley-winking-face smiley'>:)</span>");
	message = message.replace(ent.encode(";-)"), "<span class='smiley-winking-face smiley'>:)</span>");
	
	return message;
}
