/*
 * Nom : KonamiCode
 * Description : réagit à une certaine combinaison de touches et qui lance une animation et un fichier audio
 * Auteur(s) : Océane Drezet, Valentin Bouchard
 */

// Un tableau des touches autorisées
var allowedKeys =
{
  37: 'left',
  38: 'up',
  39: 'right',
  40: 'down',
  65: 'a',
  66: 'b'
};

// La séquence 'officielle' du Konami Code
var konamiCode = ['up', 'up', 'down', 'down', 'left', 'right', 'left', 'right', 'b', 'a'];

// Une variable pour se souvenir de la position de l'utilisateur dans le code
var konamiCodePosition = 0;

// Ajout un event listener pour l'appui d'une touche
document.addEventListener('keydown', function(e)
{
	// récupère la valeur de la touche dans le tableau
	var key = allowedKeys[e.keyCode];

	// récupère la valeur requise dans le Konami Code
	var requiredKey = konamiCode[konamiCodePosition];

	// compare la touche avec celle requise
	if (key == requiredKey)
	{

		// Passe l'utilisateur à la position suivante
		konamiCodePosition++;

		// Teste si la position de l'utilisateur est égale à la longueur du Konami Code
		if (konamiCodePosition == konamiCode.length)
		{
			// Lance la fonction et remet à zéro la postion de l'utilisateur
			activerPerceval();
			konamiCodePosition = 0;
			setTimeout(stopPerceval,5000);
		}
	}
	else
	{
		konamiCodePosition = 0;
	}
});

function activerPerceval()
{
	  // Joue le fichier audio donné avec une image de Perceval
	  document.getElementById('imagePerceval').classList.add('animationPerceval');
	document.getElementById('imagePerceval').innerHTML = '<div class="imagePerceval"><div>';
	document.getElementById('percevalAudio').innerHTML = '<audio src="./modules/perceval/perceval.mp3" autoplay></audio>';
}

function stopPerceval()
{
  // supprimer le fichier audio et l'image pour pouvoir le relancer
	document.getElementById('imagePerceval').classList.remove('animationPerceval');
	document.getElementById('imagePerceval').innerHTML = '';
	document.getElementById('percevalAudio').innerHTML = '';
  socket.disconnect();
}
