/*
 * Nom : Wikipedia
 * Description : 
 * Auteur(s) : Teamjojo (Baptiste, Mélanie, Jordan)
 */

 
 // Définit les méthodes "publiques" (utilisation à l'extérieur du module)
module.exports =  {
	handleWikipedia: handleWikipedia // permet d'appeler cette méthode dans server.js -> wikipedia.handleWikipedia(...)
}

/**
 * Lorsqu'on appelle Wikipedia
 */
function handleWikipedia(io, message)
{
	if (message.toLowerCase().startsWith('wiki') || message.toLowerCase().startsWith("wikipedia"))
	{
		var callSearch = message.indexOf(" ");
		var wordSearch = message.substr(callSearch, message.length);
		var request = require('request');
		
		// On appelle l'API Wikipedia en récupérant le(s) mot(s) cherché(s)
		request('https://fr.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles='+wordSearch,
			function(err, res, body)
			{  
				// On affiche les données reçues et on récupère l'ID de la première vidéo
				var result = JSON.parse(body); 
                console.log(result.query.pages);
                for (var id in result.query.pages) break;
                var def = result.query.pages[id].extract;
                var nomRecherche = result.query.pages[id].title;
                if (def != "")
                {
                    io.sockets.emit('new_message',
                    {
                        name:'Wikipedia',
                        //message: '<a target="_blank" href="'+result[3][0]+'">'+result[1][0]+'</a> : <br>'+ result[2][0] 
                        message: nomRecherche+' :<br>'+ def.substr(0, 300)+"... "+'<a target="_blank" href="https://fr.wikipedia.org/wiki/'+wordSearch+'">Wikipedia</a>'
                    });
                }
                else
                {
                    io.sockets.emit('new_message',
                    {
                        name:'Wikipedia',
                        //message: '<a target="_blank" href="'+result[3][0]+'">'+result[1][0]+'</a> : <br>'+ result[2][0] 
                        message: nomRecherche+' :<br> Aucune définition trouvée pour ce mot. <a target="_blank" href="https://fr.wikipedia.org/wiki/'+wordSearch+'">Wikipedia</a>'
                    });
                }
             }
		);	
	}	
}