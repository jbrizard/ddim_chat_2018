/**
 * Récupération de l'image téléchargée
 */
function getAvatar()
{
    // Récupérer le fichier dans la variable avatar
    var avatar = $("#avatar")[0].files[0];
    if(avatar)
    {
        // Transmet l'avatar au serveur
        socket.emit('avatar', avatar);
    }
}

// Déclencher la fonction au clic
$('#avatar').change(getAvatar);

// Gestion des événements diffusés par le serveur
socket.on('avatar_loaded', function(avatar)
{
    // Supprime le précédent avatar et le remplace par le nouveau
    $("#default-avatar").remove();
    $(".open-upload").append('<img id="default-avatar" src="' + avatar + '" alt="mon avatar"/>');
});