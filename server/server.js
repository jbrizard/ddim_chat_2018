// Chargement des dépendances
var express = require('express');	// Framework Express
var http = require('http');		// Serveur HTTP
var ioLib = require('socket.io');	// WebSocket
var ent = require('ent');		// Librairie pour encoder/décoder du HTML
var path = require('path');		// Gestion des chemins d'accès aux fichiers
var fs = require('fs');			// Accès au système de fichier

// Chargement des modules perso
var daffy = require('./modules/daffy.js');
var naziBot = require('./modules/nazibot.js');
var perceval = require('./modules/perceval.js');
var basket = require('./modules/basket.js');
var youtube = require('./modules/youtube.js');
var wikipedia = require('./modules/wikipedia.js');
var smiley = require('./modules/smiley.js');
var style = require('./modules/styletxt.js');

var giphy = require('./modules/giphy.js');
var botJoke = require('./modules/bot_joke.js');
var avatar = require('./modules/avatar.js');

// Initialisation du serveur HTTP
var app = express();
var server = http.createServer(app);

// Initialisation du websocket
var io = ioLib.listen(server);

botJoke.init(io);

// Chargement des modules perso
var daffy = require('./modules/daffy.js');
var userList = require('./modules/userList.js');
var messageHistory = require('./modules/messageHistory.js');
var messageTable = [];

// Traitement des requêtes HTTP (une seule route pour l'instant = racine)
app.get('/', function(req, res)
{
	res.sendFile(path.resolve(__dirname + '/../client/chat.html'));
});

// Traitement des fichiers "statiques" situés dans le dossier <assets> qui contient css, js, images...
app.use(express.static(path.resolve(__dirname + '/../client/assets')));

// Initialisation du module Basket
basket.init(io);

// Gestion des connexions au socket
io.sockets.on('connection', function(socket)
{
	basket.addClient(socket);
	
	// Arrivée d'un utilisateur
	socket.on('user_enter', function(name)
	{
		// Stocke le nom de l'utilisateur dans l'objet socket
		socket.name = name;
		// Lancement de la fonction d'ajout d'utilisateur
		userList.addUser(io, socket);
		messageHistory.sendHistory(io,messageTable);
		
	});
	
	// Départ d'un utilisateur
	socket.on('disconnect', function(socket)
	{
		userList.removeUser(io, socket);
	});

	// Réception d'un message
	socket.on('message', function(message)
	{
		// Par sécurité, on encode les caractères spéciaux
		message = ent.encode(message);

		// On transmet les message à la fonction addMessage
	    messageTable = messageHistory.addMessage(io,message,socket);
		
		// Remplacement des smileys claviers en emojis
		message = smiley.replaceSmileys(ent, message);
		message = style.styleTxt(ent, message);
		
		console.log(message);
		
		// Transmet le message à tous les utilisateurs (broadcast)

		io.sockets.emit('new_message', {name:socket.name, message:message, avatar:socket.avatar});

		// Transmet le message au module Daffy (on lui passe aussi l'objet "io" pour qu'il puisse envoyer des messages)

		daffy.handleDaffy(io, message);

		// Transmet le message au module Bot Joke (on lui passe aussi l'objet "io" pour qu'il puisse envoyer des messages)
		botJoke.handleBotJoke(io, message);
		
		// Transmet le message au module NaziBot (on lui passe aussi l'objet "io" pour qu'il puisse envoyer des messages)
		naziBot.handleNaziBot(io, message);

		// Transmet le message au module Perceval (on lui passe aussi l'objet "io" pour qu'il puisse envoyer des messages)
		perceval.handlePerceval(io, message);

		// Gestion API Youtube
		youtube.handleYoutube(io, message);
		
		//Gestion API Wikipedia
		wikipedia.handleWikipedia(io, message);
	});
	
	// Réception de la couleur du background
	socket.on('bgcolor', function(color)
	{
		// Transmet la nouvelle couleur à tous les utilisateurs (broadcast)
		io.sockets.emit('change_bgcolor', color);
	});

	// Recherche d'un gif
	socket.on('search_gif',function(query)
	{
		giphy.searchGif(socket, query);
	});

	//
	avatar.onConnection(socket);

	socket.on('message_gif',function(src)
	{
		
		giphy.sendGif(socket,src);
	});
});

// Lance le serveur sur le port 8080 (http://localhost:8080)
server.listen(8080);
