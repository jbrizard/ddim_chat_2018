// Connexion au socket
var socket = io.connect(':8080');

// Demande un pseudo et envoie l'info au serveur
var name = prompt('Quel est votre pseudo ?');
socket.emit('user_enter', name);

// Un utilisateur rejoint la salle
socket.on('add_user', addUser);

// Gestion des événements diffusés par le serveur
socket.on('new_message', receiveMessage);
socket.on('add_history', addHistory);
socket.on('removeLastMessage', removeLastMessage);

// Action quand on clique sur le bouton "Envoyer"
$('#send-message').click(sendMessage);

// Action quand on appuye sur la touche [Entrée] dans le champ de message (= comme Envoyer)
$('#message-input').keyup(function(evt)
{
	if (evt.keyCode == 13) // 13 = touche Entrée
		sendMessage();
});


/**
 * Envoi d'un message au serveur
 */
function sendMessage()
{
	// Récupère le message, puis vide le champ texte
	var input = $('#message-input');
	var message = input.val();
	input.val('');

	// On n'envoie pas un message vide
	if (message == '')
		return;

	// Envoi le message au serveur pour broadcast
	socket.emit('message', message);
}


function receiveMessage(data)
{
	$('#chat #messages').append(
		'<div class="message">'
		+ '<img class="avatar" src="' + data.avatar + '" alt="avatar" />'
			+ '<span class="user">' + data.name  + '</span>' 
			+ '<span class="user-message">' + data.message + '</span>'
	     + '</div>'
	)
	.scrollTop(function(){ return this.scrollHeight });  // scrolle en bas du conteneur
}

/**
 * Ajout d'un utilisateur dans la liste des participants au chat
 */
function addUser(data)
{
	// Supprimme la liste (html) des participants
	$('#participants').empty();
	// Créer la liste (html) des participants
	$('#participants').append('<p style="font-weight:bold; padding-bottom: 10px;">Participants </p>');
	data.userTable.forEach(function(value)
	{
		$('#participants').append(
			'<p>' + value + '</p>'
		);
	});
}

function addHistory(data)
{
	// Supprimme la liste (html) des participants
	$('#messages').empty();
	
	// Créer la liste (html) des participants
	data.messageTable.forEach(function(value)
	{
		console.log('boucle');
		$('#messages').append(
			 value
		);
	});
}

/**
 * Suppression du dernier message reçu par le serveur
 */
function removeLastMessage()
{
	$('.message:last').remove();
}