/*
 * Nom : userList !
 * Description : Ce module crée une liste d eparticipant dynamique en fonction des connexions et deconnexions
 * Auteur(s) : Loïc Bruez , Yannis Morel
 */

module.exports = {
	addUser: addUser, // permet d'appeler cette méthode dans server.js 
	removeUser: removeUser
}

// Le tableau qui contient des users pour les afficher
var userTable = [];

/**
 *  Ajoute un utilisateur dans la liste des participants au chat
 */
function addUser(io, socket)
{
	//Ajout des utilisateur connecté au chat dans un tableau
	userTable.push(socket.name);
	//Envoi du tableau au client
	io.sockets.emit('add_user',
	{
		userTable: userTable
	});
}

/**
 * Enleve un utilisateur dans la liste des participants au chat
 */
function removeUser(io, socket)
{
	//Suppression des utilisateur déconnecté du chat
	const index = userTable.indexOf(socket.name);
	userTable.splice(index, 1);
	//Envoi du tableau au client
	io.sockets.emit('add_user',
	{
		userTable: userTable
	});
}