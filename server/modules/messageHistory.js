/*
 * Nom : messageHistory !
 * Description : Ce module créer un historique de conversation
 * Auteur(s) : Loïc Bruez , Yannis Morel
 */

module.exports = {
	addMessage : addMessage,
	sendHistory : sendHistory
}
var fs = require('fs');

// Le tableau qui contient des users pour les afficher
var messageTable = [];



fs.readFile('history.json', 'utf8', function (err, data)
{
	if (!err)
		messageTable = JSON.parse(data);
});




/**
 *  Ajoute un utilisateur dans la liste des participants au chat
 */
function addMessage(io, message, socket)
{   
	var messageName = '<div class="message"><span class="user">'+socket.name+'</span>'+ message+'</div>';
	messageTable.push(messageName);
	
	var JsonHistory = JSON.stringify(messageTable);
	
	var fs = require('fs');
	fs.writeFile("history.json", JsonHistory, (error) => {});
	
	return messageTable;
}

/**
 *  Envoie l'historique du chat au client
 */
function sendHistory(io,messageTable)
{
	io.sockets.emit('add_history',
	{
		messageTable: messageTable
	});	
}
